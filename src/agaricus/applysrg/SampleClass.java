package agaricus.applysrg;

public class SampleClass {
    public int field1;

    public SampleClass()
    {
    }

    public void a()
    {
    }

    public int a(int var1)
    {
        return var1 * 2;
    }

    public void a(String var1)
    {
    }

    public SampleClass a(int var1, int var2)
    {
        return this;
    }
}
