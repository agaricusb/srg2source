ApplySrg2Source

Inspired by ApplySrg from https://github.com/Frans-Willem/SrgTools , but 
performs the renaming on source code instead of compiled binaries.

For porting, Minecraft, CraftBukkit, mods, plugins, etc.

## Installation

1. Install IntelliJ IDEA from http://www.jetbrains.com/idea/

2. Preferences > Plugins > Install plugin from disk... > ApplySrg2Source.jar

3. Srg > Apply Srg (new menu item), choose a .srg file, and your project will be transformed

## 

